# MealPoint

An Android application developed for TVZ's MC2 competition. Primary goal was to help foreign Erasmus exchange students in locating student canteens in Zagreb, as well as getting menus and comparative distances to them.

Documentation: https://drive.google.com/file/d/0B_4IJSFR4c7beDhxcmJ1ZHpuNnM/view?usp=sharing

Presentation: https://drive.google.com/file/d/0B_4IJSFR4c7bUFhHSlVSWExEblE/view?usp=sharing
