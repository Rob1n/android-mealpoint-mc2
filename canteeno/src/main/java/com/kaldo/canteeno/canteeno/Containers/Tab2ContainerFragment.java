package com.kaldo.canteeno.canteeno.Containers;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kaldo.canteeno.canteeno.Fragments.PhrasebookMainFragment;
import com.kaldo.canteeno.canteeno.Helpers.BaseContainerFragment;
import com.kaldo.canteeno.canteeno.R;

public class Tab2ContainerFragment extends BaseContainerFragment {
	
	private boolean mIsViewInited;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return inflater.inflate(R.layout.container_fragment, null);
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		if (!mIsViewInited) {
			mIsViewInited = true;
			initView();
		}
	}
	
	private void initView() {
		replaceFragment(new PhrasebookMainFragment(), false);
	}

}
