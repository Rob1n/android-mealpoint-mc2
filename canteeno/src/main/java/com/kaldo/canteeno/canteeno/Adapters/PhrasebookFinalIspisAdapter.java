package com.kaldo.canteeno.canteeno.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.kaldo.canteeno.canteeno.R;

import java.util.ArrayList;

/**
 * Created by Robin on 29.4.2014..
 */
public class PhrasebookFinalIspisAdapter extends ArrayAdapter<String>{

    private ArrayList<String> food_list;
    private ArrayList<String> food_list_cro;
    private LayoutInflater inflater;

    public PhrasebookFinalIspisAdapter(Context context, int resource,
                                       ArrayList<String> food_list, ArrayList<String> food_list_cro) {
        super(context, 0, food_list);

        this.food_list = food_list;
        this.food_list_cro = food_list_cro;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.phrasebook_ispis_item, null);
        }

        TextView ispisCelija = (TextView) convertView.findViewById(R.id.final_ispis);
        TextView ispisCelijaCro = (TextView) convertView.findViewById(R.id.final_ispis_cro);

        ispisCelija.setText(food_list.get(position));
        ispisCelijaCro.setText(food_list_cro.get(position));

        return convertView;
    }

}
