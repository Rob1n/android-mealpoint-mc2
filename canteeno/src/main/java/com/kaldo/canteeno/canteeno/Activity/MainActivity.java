package com.kaldo.canteeno.canteeno.Activity;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTabHost;
import android.widget.LinearLayout;
import android.widget.TabHost;

import com.kaldo.canteeno.canteeno.Containers.Tab1ContainerFragment;
import com.kaldo.canteeno.canteeno.Containers.Tab2ContainerFragment;
import com.kaldo.canteeno.canteeno.Helpers.BaseContainerFragment;
import com.kaldo.canteeno.canteeno.R;

public class MainActivity extends FragmentActivity {

    private static final String TAB_1_TAG = "Canteens";
    private static final String TAB_2_TAG = "Phrasebook";
    private FragmentTabHost mTabHost;
    boolean activeT;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
    }

    private void initView(){
        mTabHost = (FragmentTabHost) findViewById(android.R.id.tabhost);
        mTabHost.setup(this, getSupportFragmentManager(), R.id.realtabcontent);

        mTabHost.addTab(mTabHost.newTabSpec(TAB_1_TAG).setIndicator("canteens"), Tab1ContainerFragment.class, null);
        mTabHost.addTab(mTabHost.newTabSpec(TAB_2_TAG).setIndicator("phrasebook"), Tab2ContainerFragment.class, null);

        mTabHost.getTabWidget().setShowDividers(LinearLayout.SHOW_DIVIDER_NONE); // makne okomiti izmedju
        mTabHost.getTabWidget().getChildAt(0).setBackgroundResource(R.drawable.active_tab);
        mTabHost.getTabWidget().getChildAt(1).setBackgroundResource(R.drawable.inactive_tab);
        activeT = true;

        mTabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
            @Override
            public void onTabChanged(String s) {
                if (activeT){ // aktivni je drugi
                    mTabHost.getTabWidget().getChildAt(0).setBackgroundResource(R.drawable.inactive_tab);
                    mTabHost.getTabWidget().getChildAt(1).setBackgroundResource(R.drawable.active_tab);
                    activeT = false;
                }
                else {
                    mTabHost.getTabWidget().getChildAt(0).setBackgroundResource(R.drawable.active_tab);
                    mTabHost.getTabWidget().getChildAt(1).setBackgroundResource(R.drawable.inactive_tab);
                    activeT = true;
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        boolean isPopFragment = false;
        String currentTabTag = mTabHost.getCurrentTabTag();
        if (currentTabTag.equals(TAB_1_TAG)) {
            isPopFragment = ((BaseContainerFragment)getSupportFragmentManager().findFragmentByTag(TAB_1_TAG)).popFragment();
        } else if (currentTabTag.equals(TAB_2_TAG)) {
            isPopFragment = ((BaseContainerFragment)getSupportFragmentManager().findFragmentByTag(TAB_2_TAG)).popFragment();
        }
        if (!isPopFragment) {
            finish();
        }
    }

}
