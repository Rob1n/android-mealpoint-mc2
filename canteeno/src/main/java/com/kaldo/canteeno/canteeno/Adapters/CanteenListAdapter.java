package com.kaldo.canteeno.canteeno.Adapters;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.kaldo.canteeno.canteeno.Fragments.CanteenInfoFragment;
import com.kaldo.canteeno.canteeno.Items.Canteen;
import com.kaldo.canteeno.canteeno.R;

import java.util.ArrayList;


public class CanteenListAdapter extends ArrayAdapter<Canteen>{

    FragmentManager fragmentManager;
    private ArrayList<Canteen> canteen_list;
    private LayoutInflater inflater;

    public CanteenListAdapter(Context context, int resource, ArrayList<Canteen> objects, FragmentManager fragmentManager) {
        super(context, 0, objects);
        this.canteen_list = objects;
        inflater = LayoutInflater.from(context);
        this.fragmentManager = fragmentManager;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.canteen_list_item, null);
        }
        // init
        TextView canteen_naziv = (TextView) convertView.findViewById(R.id.canteen_item_naziv);
        //TextView canteen_adresa = (TextView) convertView.findViewById(R.id.canteen_item_adresa);
        ImageView btn_canteen_info = (ImageView) convertView.findViewById(R.id.btn_canteen_info);

        // data
        final Canteen canteen_temp = getItem(position);
        canteen_naziv.setText(canteen_temp.getNaziv());
        //canteen_adresa.setText(canteen_temp.getAdresa());
        notifyDataSetChanged();

        // button
        btn_canteen_info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                CanteenInfoFragment fragment = new CanteenInfoFragment(canteen_temp);
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.addToBackStack(null);
                transaction.replace(R.id.container_framelayout, fragment);
                transaction.commit();
            }
        });

        return convertView;
    }

    @Override
    public Canteen getItem(int position) {
        return canteen_list.get(position);
    }

    @Override
    public int getCount() {
        return canteen_list.size();
    }
}
