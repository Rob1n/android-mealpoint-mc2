package com.kaldo.canteeno.canteeno.Fragments;

import android.animation.ArgbEvaluator;
import android.animation.ValueAnimator;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTabHost;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.kaldo.canteeno.canteeno.Activity.IspisActivity;
import com.kaldo.canteeno.canteeno.Adapters.PhrasebookFoodAdapter;
import com.kaldo.canteeno.canteeno.Helpers.CollapseAnimation;
import com.kaldo.canteeno.canteeno.Items.Food;
import com.kaldo.canteeno.canteeno.Items.Ispis;
import com.kaldo.canteeno.canteeno.R;

import java.util.ArrayList;


public class PhrasebookMainFragment extends Fragment implements View.OnClickListener{


    private ListView listView;
    private ArrayList<Food> food_list;
    private ArrayList<Food> filtered_list;
    private PhrasebookFoodAdapter adapter;

    ImageView img_btn_all;
    ImageView img_btn_food;
    ImageView img_btn_drink;
    ImageView img_btn_sweets;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.main_phrasebook, null);
        initView(view);
        return view;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
	}
	
	private void initView(View view) {

        final View bg = view.findViewById(R.id.phrasebook_bg);

        img_btn_all = (ImageView) view.findViewById(R.id.phrasebook_btn_all);
        img_btn_food = (ImageView) view.findViewById(R.id.phrasebook_btn_food);
        img_btn_drink = (ImageView) view.findViewById(R.id.phrasebook_btn_drinks);
        img_btn_sweets = (ImageView) view.findViewById(R.id.phrasebook_btn_sweets);

        food_list = new ArrayList<Food>();
        populateList();
        filtered_list = new ArrayList<Food>();

        listView = (ListView) view.findViewById(R.id.list_food_picker);
        adapter = new PhrasebookFoodAdapter(view.getContext(), 0, filtered_list);
        listView.setAdapter(adapter);

        filterBy(filtered_list, 'A');

        final boolean[] collapsed = {true};
        final View toolbar = view.findViewById(R.id.collapseToolbar);
        ImageView izbrisi = (ImageView) view.findViewById(R.id.btn_ocisti_ponudu);
        ImageView ispisi = (ImageView) view.findViewById(R.id.btn_ispisi_ponudu);
        final TextView marked = (TextView) view.findViewById(R.id.phrasebook_nrOfMarked);

        izbrisi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                for (int i=0; i<food_list.size(); i++){
                    food_list.get(i).setChecked(false);
                }
                adapter.notifyDataSetChanged();
                CollapseAnimation expandAni = new CollapseAnimation(toolbar, 150);
                toolbar.startAnimation(expandAni);
                collapsed[0] = true;
            }
        });

        ispisi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ArrayList<String> zaIspis = new ArrayList<String>();
                ArrayList<String> zaIspis_eng = new ArrayList<String>();
                for (int i = 0; i < food_list.size(); i++) {
                    if (food_list.get(i).isChecked()){
                        zaIspis.add(food_list.get(i).getCroIme());
                        zaIspis_eng.add(food_list.get(i).getIme());
                    }
                }
                Ispis ispisi = new Ispis(zaIspis, zaIspis_eng);
                Intent intent = new Intent(getActivity(), IspisActivity.class);
                intent.putExtra("IspisPonude", ispisi);

                startActivity(intent);
            }
        });

        marked.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ArrayList<String> zaIspis = new ArrayList<String>();
                ArrayList<String> zaIspis_eng = new ArrayList<String>();
                for (int i = 0; i < food_list.size(); i++) {
                    if (food_list.get(i).isChecked()){
                        zaIspis.add(food_list.get(i).getCroIme());
                        zaIspis_eng.add(food_list.get(i).getIme());
                    }
                }
                Ispis ispisi = new Ispis(zaIspis, zaIspis_eng);
                Intent intent = new Intent(getActivity(), IspisActivity.class);
                intent.putExtra("IspisPonude", ispisi);

                startActivity(intent);
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, final View view, int position, long id) {

                filtered_list.get(position).setChecked(!filtered_list.get(position).isChecked());
                adapter.notifyDataSetChanged();
                int numOC = numberOfChecks();

                if ((numOC == 1 && !(toolbar.getVisibility() == View.VISIBLE))
                        || (numOC == 0 && (toolbar.getVisibility() == View.VISIBLE))) {
                    CollapseAnimation expandAni = new CollapseAnimation(toolbar, 150);
                    toolbar.startAnimation(expandAni);
                }
                if (numOC == 1){
                    marked.setText(numOC + " item marked");
                } else {
                    marked.setText(numOC + " items marked");
                }
            }
        });

        img_btn_all.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeColor(R.id.phrasebook_bg, bg, "#e2f4fb");
                colorButtons(0);
                filterBy(filtered_list, 'A');
            }
        });

        img_btn_food.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeColor(R.id.phrasebook_bg, bg, "#f5eafa");
                colorButtons(1);
                filterBy(filtered_list, 'F');
            }
        });

        img_btn_drink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeColor(R.id.phrasebook_bg, bg, "#ffe4e4");
                colorButtons(2);
                filterBy(filtered_list, 'D');
            }
        });

        img_btn_sweets.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeColor(R.id.phrasebook_bg, bg, "#fff6df");
                colorButtons(3);
                 filterBy(filtered_list, 'S');
            }
        });
	}

    private void changeColor(int resource, final View item, String color){
        Integer colorFrom = getResources().getColor(resource);
        Integer colorTo = Color.parseColor(color);
        ValueAnimator colorAnimation = ValueAnimator.ofObject(new ArgbEvaluator(), colorFrom, colorTo);
        colorAnimation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animator) {
                item.setBackgroundColor((Integer)animator.getAnimatedValue());
            }
        });
        colorAnimation.start();
    }

    private void colorButtons(int num){
        img_btn_all.setBackgroundColor(Color.parseColor("#4b4b4b")); //33b5e5
        img_btn_food.setBackgroundColor(Color.parseColor("#4b4b4b")); //aa66cc
        img_btn_drink.setBackgroundColor(Color.parseColor("#4b4b4b")); //ff4444
        img_btn_sweets.setBackgroundColor(Color.parseColor("#4b4b4b")); //ffbb33
        switch (num){
            case 0: img_btn_all.setBackgroundColor(Color.parseColor("#33b5e5")); break;
            case 1: img_btn_food.setBackgroundColor(Color.parseColor("#aa66cc")); break;
            case 2: img_btn_drink.setBackgroundColor(Color.parseColor("#ff4444")); break;
            case 3: img_btn_sweets.setBackgroundColor(Color.parseColor("#ffbb33"));
        }
    }

    private void filterBy(ArrayList<Food> filtered_list, char category){
        filtered_list.clear();
        if (category == 'A'){
            for (int i=0; i<food_list.size(); i++){
                filtered_list.add(food_list.get(i));
            }
            adapter.notifyDataSetChanged();
            return;
        } else {
            for (int i = 0; i < food_list.size(); i++) {
                if (food_list.get(i).getCategory() == category) {
                    filtered_list.add(food_list.get(i));
                }
            }
            adapter.notifyDataSetChanged();
            return;
        }
    }

    private void populateList(){
        food_list.add(new Food("Apple", 'S', "Jabuka"));
        food_list.add(new Food("Apple juice", 'D', "Sok jabuka"));
        food_list.add(new Food("Apricot juice", 'D', "Sok marelica"));
        food_list.add(new Food("Baklava", 'S', "Baklava"));
        food_list.add(new Food("Banana", 'S', "Banana"));
        food_list.add(new Food("Banana cubes", 'S', "Banana kocke"));
        food_list.add(new Food("Black Forest cake", 'S', "Schwarzwald torta"));
        food_list.add(new Food("Blueberry juice", 'D', "Sok borovnica"));
        food_list.add(new Food("Boiled egg", 'F', "Kuhano jaje"));
        food_list.add(new Food("Bread (slice)", 'F', "Kruh (šnita)"));
        food_list.add(new Food("Buchtel", 'S', "Buhtla"));
        food_list.add(new Food("Burger", 'F', "Pljeskavica"));
        food_list.add(new Food("Burger with cheese", 'F', "Pljeskavica sa sirom"));
        food_list.add(new Food("Cabbage rolls with minced meat", 'F', "Sarma"));
        food_list.add(new Food("Cao Cao", 'S', "Cao Cao"));
        food_list.add(new Food("Cheese burek", 'F', "Burek sa sirom"));
        food_list.add(new Food("Cherry juice", 'D', "Sok višnja"));
        food_list.add(new Food("Chicken", 'F', "Piletina"));
        food_list.add(new Food("Chocolate cake", 'S', "Čokoladna torta"));
        food_list.add(new Food("Chocolate milk", 'D', "Čokoladno mlijeko"));
        food_list.add(new Food("Chop", 'F', "Kotlet"));
        food_list.add(new Food("Chutney", 'F', "Ajvar"));
        food_list.add(new Food("Cocktail juice", 'D', "Sok coctail"));
        food_list.add(new Food("Cocoa", 'D', "Kakao"));
        food_list.add(new Food("Coconut cubes", 'S', "Kokos kocke"));
        food_list.add(new Food("Cooked vegetables", 'F', "Kuhano povrće"));
        food_list.add(new Food("Cordon Bleu", 'F', "Zagrebački odrezak"));
        food_list.add(new Food("Cream", 'F', "Vrhnje"));
        food_list.add(new Food("Cream Cakes", 'S', "Kremšnita"));
        food_list.add(new Food("Croissant", 'S', "Kroasan"));
        food_list.add(new Food("Croquettes", 'F', "Kroketi"));
        food_list.add(new Food("Doughnut", 'S', "Krafna"));
        food_list.add(new Food("Fish", 'F', "Riba"));
        food_list.add(new Food("French fries", 'F', "Pomfrit"));
        food_list.add(new Food("Fried cheese", 'F', "Pohani sir"));
        food_list.add(new Food("Fried vegetables", 'F', "Prženo povrće"));
        food_list.add(new Food("Fruit compote", 'S', "Kompot voćni"));
        food_list.add(new Food("Fruit cubes", 'S', "Voćne kocke"));
        food_list.add(new Food("Fruit yogurt", 'S', "Voćni jogurt"));
        food_list.add(new Food("Gnocchi", 'F', "Njoki"));
        food_list.add(new Food("Goulash", 'F', "Gulaš"));
        food_list.add(new Food("Grilled minced meat", 'F', "Ćevapi"));
        food_list.add(new Food("Grinders", 'F', "Mlinci"));
        food_list.add(new Food("Hamburger", 'F', "Hamburger"));
        food_list.add(new Food("Honey", 'S', "Med"));
        food_list.add(new Food("Horseradish", 'F', "Hren"));
        food_list.add(new Food("Hot dog", 'F', "Hot dog"));
        food_list.add(new Food("Ice cream", 'S', "Sladoled"));
        food_list.add(new Food("Icy wind", 'S', "Ledeni vjetar"));
        food_list.add(new Food("Jaffa cubes", 'S', "Jaffa kocke"));
        food_list.add(new Food("Ketchup", 'F', "Ketchup"));
        food_list.add(new Food("Kinder Pingui", 'S', "Kinder Pingui"));
        food_list.add(new Food("Lasagna", 'F', "Lazanje"));
        food_list.add(new Food("Light stew made of vegetables", 'F', "Sataraš"));
        food_list.add(new Food("Milk", 'D', "Mlijeko"));
        food_list.add(new Food("Madjarica", 'S', "Mađarica"));
        food_list.add(new Food("Makovnjacha", 'S', "Makovnjača"));
        food_list.add(new Food("Marble cake", 'S', "Mramorni kolač"));
        food_list.add(new Food("Marmalade", 'S', "Marmelada"));
        food_list.add(new Food("Mayonnaise", 'F', "Majoneza"));
        food_list.add(new Food("Meatballs", 'F', "Mesne okruglice"));
        food_list.add(new Food("Milk spread", 'F', "Mliječni namaz"));
        food_list.add(new Food("Moussaka", 'F', "Musaka"));
        food_list.add(new Food("Muffin", 'S', "Muffin"));
        food_list.add(new Food("Multivitamin juice", 'D', "Sok multivitamin"));
        food_list.add(new Food("Munchmallow", 'S', "Munchmallow"));
        food_list.add(new Food("Mustard", 'F', "Senf"));
        food_list.add(new Food("Orange", 'S', "Naranča"));
        food_list.add(new Food("Orange juice", 'D', "Sok naranča"));
        food_list.add(new Food("Orehnjacha", 'S', "Orehnjača"));
        food_list.add(new Food("Parfait cubes", 'S', "Parfe kocke"));
        food_list.add(new Food("Parmesan", 'F', "Parmezan"));
        food_list.add(new Food("Pasta", 'F', "Tjestenina"));
        food_list.add(new Food("Pate", 'F', "Pašteta"));
        food_list.add(new Food("Peach juice", 'D', "Sok breskva"));
        food_list.add(new Food("Pear juice", 'D', "Sok kruška"));
        food_list.add(new Food("Pie", 'S', "Pita"));
        food_list.add(new Food("Pirogue with cheese", 'S', "Piroška sa sirom"));
        food_list.add(new Food("Pizza", 'F', "Pizza"));
        food_list.add(new Food("Polenta", 'F', "Žganci"));
        food_list.add(new Food("Pork", 'F', "Svinjetina"));
        food_list.add(new Food("Potato", 'F', "Krumpir"));
        food_list.add(new Food("Princess donut", 'S', "Princez krafna"));
        food_list.add(new Food("Pudding", 'S', "Puding"));
        food_list.add(new Food("Raffaello", 'S', "Raffaello"));
        food_list.add(new Food("Rice", 'F', "Riža"));
        food_list.add(new Food("Risotto", 'F', "Rižoto"));
        food_list.add(new Food("Rolls", 'S', "Pecivo"));
        food_list.add(new Food("Sachertorte", 'S', "Sacher torta"));
        food_list.add(new Food("Salad", 'F', "Salata"));
        food_list.add(new Food("Sandwich", 'F', "Sendvič"));
        food_list.add(new Food("Sauce", 'F', "Umak"));
        food_list.add(new Food("Sausages", 'F', "Kobasice/hrenovke"));
        food_list.add(new Food("Skewers", 'F', "Ražnjići"));
        food_list.add(new Food("Soup", 'F', "Juha"));
        food_list.add(new Food("Soy", 'F', "Soja"));
        food_list.add(new Food("Squid", 'F', "Lignje"));
        food_list.add(new Food("Steak", 'F', "Odrezak"));
        food_list.add(new Food("Stew", 'F', "Varivo"));
        food_list.add(new Food("Strawberry juice", 'D', "Sok jagoda"));
        food_list.add(new Food("Strudel", 'S', "Štrudel"));
        food_list.add(new Food("Strukle", 'S', "Štrukle"));
        food_list.add(new Food("Tangerine", 'S', "Mandarina"));
        food_list.add(new Food("Tartar sauce", 'F', "Tartar"));
        food_list.add(new Food("Tiramisu", 'S', "Tiramisu"));
        food_list.add(new Food("Tortellini", 'F', "Tortellini"));
        food_list.add(new Food("Turkey", 'F', "Puretina"));
        food_list.add(new Food("Tea", 'D', "Čaj"));
        food_list.add(new Food("Veal", 'F', "Junetina/teletina"));
        food_list.add(new Food("Water", 'D', "Voda"));
        food_list.add(new Food("Watermelon", 'S', "Lubenica"));
        food_list.add(new Food("White coffee", 'D', "Bijela kava"));
        food_list.add(new Food("Yogurt", 'D', "Jogurt"));

    }

    private int numberOfChecks() {
        int br = 0;
        for (int i = 0; i < food_list.size(); i++) {
            if (food_list.get(i).isChecked()){
                br++;
            }
        }
        return br;
    }

    @Override
    public void onClick(View view) {

    }

}
