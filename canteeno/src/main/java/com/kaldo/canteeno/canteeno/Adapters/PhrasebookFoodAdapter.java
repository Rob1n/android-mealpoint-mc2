package com.kaldo.canteeno.canteeno.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.kaldo.canteeno.canteeno.Items.Food;
import com.kaldo.canteeno.canteeno.R;

import java.util.ArrayList;

/**
 * Created by Robin on 28.4.2014..
 */
public class PhrasebookFoodAdapter extends ArrayAdapter<Food> {

    private LayoutInflater inflater;

    private ArrayList<Food> food_list;

    public PhrasebookFoodAdapter(Context context, int resource, ArrayList<Food> objects) {
        super(context, 0, objects);
        this.food_list = objects;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.phrasebook_food_item, null);
        }

        TextView ime = (TextView) convertView.findViewById(R.id.ime_hrane_TV);
        final Food current = food_list.get(position);
        ime.setText(current.getIme());
        if (current.isChecked()){
            convertView.setBackgroundColor(Color.parseColor("#50c0e9"));
            ime.setTextColor(Color.parseColor("#FFFFFF"));
        }else {
            convertView.setBackgroundColor(Color.parseColor("#FFFFFF"));
            ime.setTextColor(Color.parseColor("#000000"));
        }
        notifyDataSetChanged();
        return convertView;
    }

    private int numberOfChecks() {
        int br = 0;
        for (int i = 0; i < food_list.size(); i++) {
            if (food_list.get(i).isChecked()){
                br++;
            }
        }
        return br;
    }
}
