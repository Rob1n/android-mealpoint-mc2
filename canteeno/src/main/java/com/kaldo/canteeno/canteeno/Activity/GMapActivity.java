package com.kaldo.canteeno.canteeno.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.kaldo.canteeno.canteeno.Helpers.GMapV2Direction;
import com.kaldo.canteeno.canteeno.Items.Canteen;
import com.kaldo.canteeno.canteeno.R;

import org.w3c.dom.Document;

import java.util.ArrayList;
import java.util.List;


public class GMapActivity extends FragmentActivity{

    String distance1;
    double DoubleDistance = 0;
    String distance;

    Document document;
    GMapV2Direction v2GetRouteDirection;
    LatLng fromPosition;
    LatLng toPosition;
    GoogleMap mGoogleMap;
    MarkerOptions markerOptions;

    Canteen canteen;
    LatLng myLocation;
    LocationManager mLocationManager;

    SupportMapFragment supportMapFragment;

    GetRouteTask getRoute;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.canteen_map_activity);

        Intent i = getIntent();
        this.canteen = (Canteen) i.getParcelableExtra("ParCanteen");

        mGoogleMap = null;
        setUpMapIfNeeded();
    }

    private Location getLastKnownLocation() {
        mLocationManager = (LocationManager) getApplicationContext().getSystemService(LOCATION_SERVICE);
        List<String> providers = mLocationManager.getProviders(true);
        Location bestLocation = null;
        for (String provider : providers) {
            Location l = mLocationManager.getLastKnownLocation(provider);

            if (l == null) {
                continue;
            }
            if (bestLocation == null
                    || l.getAccuracy() < bestLocation.getAccuracy()) {
                bestLocation = l;
            }
        }
        if (bestLocation == null) {
            return null;
        }
        return bestLocation;
    }

    private class GetRouteTask extends AsyncTask<String, Void, String> {

        private ProgressDialog Dialog;
        String response = "";
        @Override
        protected void onPreExecute() {
            //Dialog = new ProgressDialog(GMapActivity.this);
            //Dialog.setMessage("Loading route...");
            //Dialog.show();
        }

        @Override
        protected String doInBackground(String... urls) {
            //Get All Route values
            document = v2GetRouteDirection.getDocument(fromPosition, toPosition, GMapV2Direction.MODE_WALKING);
            //get distance and convert it in double
            distance = v2GetRouteDirection.getDistanceText(document).toString();
            distance1 = distance.substring(0,3);
            DoubleDistance = Double.parseDouble(distance1);
            response = "Success";
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            mGoogleMap.clear();
            if(response.equalsIgnoreCase("Success")){
                ArrayList<LatLng> directionPoint = v2GetRouteDirection.getDirection(document);
                PolylineOptions rectLine = new PolylineOptions().width(8).color(
                        Color.RED);

                for (int i = 0; i < directionPoint.size(); i++) {
                    rectLine.add(directionPoint.get(i));
                }
                // Adding route on the map
                mGoogleMap.addPolyline(rectLine);
                markerOptions.position(toPosition);
                markerOptions.draggable(false);
                markerOptions.title(canteen.getNaziv());
                markerOptions.snippet(canteen.getSnippet());
                mGoogleMap.addMarker(markerOptions);
            }
            //Dialog.dismiss();
            TextView dist = (TextView) findViewById(R.id.distance);
            dist.setText(String.format("Distance: " + DoubleDistance + " km"));
        }
    }

    private void setUpMapIfNeeded() {
        if (mGoogleMap == null) {
            mGoogleMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                    .getMap();
            if (mGoogleMap != null) {
                setUpMap();
            }
        }
    }

    private void setUpMap(){
        v2GetRouteDirection = new GMapV2Direction();
        supportMapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mGoogleMap = supportMapFragment.getMap();

        mGoogleMap.getUiSettings().setZoomControlsEnabled(true);
        mGoogleMap.getUiSettings().setCompassEnabled(true);
        mGoogleMap.getUiSettings().setMyLocationButtonEnabled(true);
        mGoogleMap.getUiSettings().setAllGesturesEnabled(true);
        mGoogleMap.setTrafficEnabled(false);
        mGoogleMap.animateCamera(CameraUpdateFactory.zoomTo(12));

        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(canteen.getLocation(), 12));
        markerOptions = new MarkerOptions();
        mGoogleMap.setMyLocationEnabled(true);

        // get location
        Location Location = getLastKnownLocation();
        myLocation = new LatLng(Location.getLatitude(), Location.getLongitude());
        fromPosition = myLocation;
        toPosition = canteen.getLocation();


        // alt get location

        // Acquire a reference to the system Location Manager
        LocationManager locationManager = (LocationManager) this.getSystemService(LOCATION_SERVICE);
        // Define a listener that responds to location updates
        LocationListener locationListener = new LocationListener() {
            public void onLocationChanged(Location location) {
                // Called when a new location is found by the network location provider.
                fromPosition = new LatLng(location.getLatitude(), location.getLongitude());
                GetRouteTask getRoute = new GetRouteTask();
                getRoute.execute();
            }

            public void onStatusChanged(String provider, int status, Bundle extras) {
            }

            public void onProviderEnabled(String provider) {
            }

            public void onProviderDisabled(String provider) {
            }
        };
        // Register the listener with the Location Manager to receive location updates
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 30, 0, locationListener);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 10, 0, locationListener);

        fromPosition = myLocation;
        //toPosition = canteen.getLocation();

        getRoute = new GetRouteTask();
        getRoute.execute();
    }

    @Override
    protected void onStop() {
        super.onStop();
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setUpMapIfNeeded();
    }


}
