package com.kaldo.canteeno.canteeno.Fragments;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.android.gms.maps.model.LatLng;
import com.kaldo.canteeno.canteeno.Adapters.CanteenListAdapter;
import com.kaldo.canteeno.canteeno.Helpers.BaseContainerFragment;
import com.kaldo.canteeno.canteeno.Items.Canteen;
import com.kaldo.canteeno.canteeno.R;

import java.util.ArrayList;
import java.util.Arrays;

public class CanteensMainFragment extends BaseContainerFragment {

    private ListView listView;
    private ArrayList<Canteen> object_list;
    private CanteenListAdapter adapter;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.main_canteen_list, null);
        initView(view);
        return view;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
	}
	
	private void initView(View view) {
        listView = (ListView) view.findViewById(R.id.canteen_list);
        object_list = new ArrayList<Canteen>();

        adapter = new CanteenListAdapter(view.getContext(), 0, object_list, getFragmentManager());
        listView.setAdapter(adapter);

        if (object_list.size() == 0) {
            populateList();
        }
        adapter.notifyDataSetChanged();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                CanteenInfoFragment fragment = new CanteenInfoFragment(object_list.get(position));

                FragmentTransaction transaction = getFragmentManager().beginTransaction(); //getChildFragmentManager();
                transaction.addToBackStack(null);
                transaction.replace(R.id.container_framelayout, fragment);

                transaction.commit();
            }
        });

	}
	
	private void replaceFragment() {
		//((BaseContainerFragment)getParentFragment()).replaceFragment(new Tab1AddOnFragment(), true);
        //((BaseContainerFragment)getParentFragment()).replaceFragment(new PhrasebookCategoryFragment(), true);
	}

    private void populateList(){
        ArrayList<String> tip;
        ArrayList<String> radnoVrijeme;
        ArrayList<String> ponuda;

        { // SC SAVSKA
            Canteen m_savska = new Canteen("SC Savska", "Savska 25", "Savska 25",
                    new LatLng(45.803772, 15.96645), R.drawable.sc);
            tip = new ArrayList<String>(Arrays.asList(
                    "Restaurant 'Savska'",
                    "Express restaurant"
            ));
            radnoVrijeme = new ArrayList<String>(Arrays.asList(
                    "Lunch (left line) 10:00 – 16:00" +
                        "\nLunch (right line) 11:30 – 16:00" +
                        "\nDinner (left line) 17:30 – 20:30" +
                        "\nDoesn't work during the weekend.",
                    "Mon-Sat: 9:00 – 15:00" +
                        "\nMon-Sat: 16:00 – 21:00" +
                        "\nSat: 9:00 – 21:00" +
                        "\nDoesn't work on Sundays"
            ));
            ponuda = new ArrayList<String>(Arrays.asList(
                    "Menus\nChoice meals",
                    "Grilled meals\nPizza\nSandwitches"
            ));
            m_savska.setInfo(tip, radnoVrijeme, ponuda);
            object_list.add(m_savska);
        } // SC SAVSKA
        { // SAVA
            Canteen m_sava = new Canteen("Sava", "Jarunska 2", "Jarunska 2",
                    new LatLng(45.785115, 15.948606), R.drawable.ic_launcher);
            tip = new ArrayList<String>(Arrays.asList(
                    "Restaurant I",
                    "Restaurant II",
                    "Pizzeria",
                    "Pastry shop"
            ));
            radnoVrijeme = new ArrayList<String>(Arrays.asList(
                    "Breakfast: 7:00 – 9:30" +
                            "\nSat: (Breakfast) 8:00 – 10:00 " +
                            "\nLunch: 11:30 – 16:00" +
                            "\nDinner: 17:30 – 21:00" +
                            "\nEvery other Friday the restaurant doesn't work the whole day",
                    "Lunch: 12:00 – 16:30" +
                            "\nDinner: 18:00 – 21:30" +
                            "\nEvery other Friday the restaurant doesn't work the whole day",
                    "Mon-Fri: 9:30 – 15:00" +
                            "\nMon-Fri: 16:30 - 20:30" +
                            "\nDoesn't work on weekends.",
                    "Mon-Sat: 11:00 – 16:00" +
                            "\nMon-Sat: 17:00 – 22:00" +
                            "\nDoesn't work on Sundays."
            ));
            ponuda = new ArrayList<String>(Arrays.asList(
                    "Menus\nChoice meals",
                    "Menus\nChoice meals",
                    "Fast food",
                    "Icecream\nSweets"
            ));

            m_sava.setInfo(tip, radnoVrijeme, ponuda);
            object_list.add(m_sava);
        } // SAVA
        { // CVJETNO
            Canteen m_cvjetno = new Canteen("Cvjetno", "Odranska 8", "Odranska 8",
                    new LatLng(45.793565, 15.963139), R.drawable.ic_launcher);
            tip = new ArrayList<String>(Arrays.asList(
                    "Line I",
                    "Line II",
                    "Café bar 'Cvjetno'"
            ));
            radnoVrijeme = new ArrayList<String>(Arrays.asList(
                    "Lunch: 11:00 – 16:00" +
                            "\nDinner: 17:30 – 21:00" +
                            "\nWorks every day except on Monday during lunch",
                    "Breakfast: 7:00 – 10:00" +
                            "\nLunch: 12:00 – 15:00" +
                            "\nDinner: 17:30 – 20:00" +
                            "\nOn weekends and holidays:" +
                            "\n\tBreakfast: 8:00 – 10:00" +
                            "\n\tLine II doesn't work during dinner" +
                            "\nWorks every day except on Monday during lunch",
                    "Mon-Fri: 8:00 – 21:00" +
                            "\nSelling sweets: 11:00 – 21:00" +
                            "\nDoesn't work on weekends and holidays."
            ));
            ponuda = new ArrayList<String>(Arrays.asList(
                    "Menus\nChoice meals",
                    "Menus\nChoice meals",
                    "Icecream\nSweets"
            ));
            m_cvjetno.setInfo(tip, radnoVrijeme, ponuda);
            object_list.add(m_cvjetno);
        } // CVJETNO
        { // LASCINA
            Canteen m_lascina = new Canteen("Lašćina", "Laščinska cesta 32", "Laščinska cesta 32",
                    new LatLng(45.821570, 16.000406), R.drawable.ic_launcher);
            tip = new ArrayList<String>(Arrays.asList(
                    "Restaurant 'Lašćina'"
            ));
            radnoVrijeme = new ArrayList<String>(Arrays.asList(
                    "Breakfast: 8:00 – 9:30" +
                            "\nLunch: 11:00 – 15:30" +
                            "\nDinner: 17:30 – 20:30" +
                            "\nSaturday Dinner: 17:30 – 20:00" +
                            "\nDoesn't work on Sundays."
            ));
            ponuda = new ArrayList<String>(Arrays.asList(
                    "Menus\nChoice meals"
            ));
            m_lascina.setInfo(tip, radnoVrijeme, ponuda);
            object_list.add(m_lascina);
        } // LASCINA
        { // BORONGAJ
            Canteen m_borongaj = new Canteen("Borongaj", "Borongajska cesta 83f", "objekt 43",
                    new LatLng(45.809560, 16.022505), R.drawable.ic_launcher);
            tip = new ArrayList<String>(Arrays.asList(
                    "Restaurant",
                    "Café bar 'Borongaj'"
            ));
            radnoVrijeme = new ArrayList<String>(Arrays.asList(
                    "Breakfast (left line): 7:30 – 10:30" +
                            "\nMon-Wed Lunch: 10:00 – 14:00" +
                            "\nFri Lunch: 10:00 – 14:30" +
                            "\nDoesn't work on weekends.",
                    "Mon-Wed: 8:00 – 17:00" +
                            "\nFri: 8:00 – 15:00" +
                            "\nDoesn't work on weekends."
            ));
            ponuda = new ArrayList<String>(Arrays.asList(
                    "Menus\nChoice meals",
                    "Beverages"
            ));
            m_borongaj.setInfo(tip, radnoVrijeme, ponuda);
            object_list.add(m_borongaj);
        } // BORONGAJ
        { // EKONOMIJA
            Canteen m_ekonomija = new Canteen("EFZG", "Trg J. F. Kennedyja 6", "Trg J. F. Kennedyja 6",
                    new LatLng(45.816096, 16.011489), R.drawable.ic_launcher);
            tip = new ArrayList<String>(Arrays.asList(
                    "Restaurant 'Ekonomija'",
                    "Restaurant 'Kefa'"
            ));
            radnoVrijeme = new ArrayList<String>(Arrays.asList(
                    "11:00 – 15:00",
                    "7:30 – 20:00" +
                            "\nFound on the upper floor of the building."
            ));
            ponuda = new ArrayList<String>(Arrays.asList(
                    "Menus\nChoice meals",
                    "Fast food"
            ));
            m_ekonomija.setInfo(tip, radnoVrijeme, ponuda);
            object_list.add(m_ekonomija);
        } // EKONOMIJA
        { // MEDICINA
        Canteen m_medicina = new Canteen("Medicina", "Šalata 3", "Šalata 3",
                new LatLng(45.818996, 15.984214), R.drawable.ic_launcher);
        tip = new ArrayList<String>(Arrays.asList(
                "Restaurant 'Medicina'"
        ));
        radnoVrijeme = new ArrayList<String>(Arrays.asList(
                "Mon-Fri: 7:30 – 18:00"
        ));
        ponuda = new ArrayList<String>(Arrays.asList(
                "Menus\nChoice meals"
        ));
        m_medicina.setInfo(tip, radnoVrijeme, ponuda);
        object_list.add(m_medicina);
    } // MEDICINA
        { // VETERINA
            Canteen m_veterina = new Canteen("Veterina", "Heinzelova ulica 55", "Heinzelova ulica 55",
                    new LatLng(45.805305, 16.008666), R.drawable.ic_launcher);
            tip = new ArrayList<String>(Arrays.asList(
                    "Restaurant 'Veterina'"
            ));
            radnoVrijeme = new ArrayList<String>(Arrays.asList(
                    "Mon-Fri: 8:00 – 15:00"
            ));
            ponuda = new ArrayList<String>(Arrays.asList(
                    "Menus\nChoice meals"
            ));
            m_veterina.setInfo(tip, radnoVrijeme, ponuda);
            object_list.add(m_veterina);
        } // VETERINA
        { // SUMARSTVO
            Canteen m_sumarstvo = new Canteen("Šumarstvo", "Svetošimunska cesta 25", "Svetošimunska cesta 25",
                    new LatLng(45.831603, 16.029647), R.drawable.ic_launcher);
            tip = new ArrayList<String>(Arrays.asList(
                    "Restaurant 'Šumarstvo'"
            ));
            radnoVrijeme = new ArrayList<String>(Arrays.asList(
                    "Mon-Fri: 8:00 – 15:00"
            ));
            ponuda = new ArrayList<String>(Arrays.asList(
                    "Menus\nChoice meals"
            ));
            m_sumarstvo.setInfo(tip, radnoVrijeme, ponuda);
            object_list.add(m_sumarstvo);
        } // SUMARSTVO
        { // FSB
            Canteen m_fsb = new Canteen("FSB", "Ulica Ivana Lučića 5", "Ulica Ivana Lučića 5",
                    new LatLng(45.795372, 15.971229), R.drawable.sc);
            tip = new ArrayList<String>(Arrays.asList(
                    "Restaurant 'FSB'"
            ));
            radnoVrijeme = new ArrayList<String>(Arrays.asList(
                    "8:00 - 16:00" +
                            "\nDoesn't work during the weekend."
                    ));
            ponuda = new ArrayList<String>(Arrays.asList(
                    "Choice meals"
                    ));
            m_fsb.setInfo(tip, radnoVrijeme, ponuda);
            object_list.add(m_fsb);
        } // FSB
        { // ALU
            Canteen m_alu = new Canteen("ALU", "Ilica 85", "Ilica 85",
                    new LatLng(45.811591, 15.962646), R.drawable.sc);
            tip = new ArrayList<String>(Arrays.asList(
                    "Restaurant 'ALU'"
                    ));
            radnoVrijeme = new ArrayList<String>(Arrays.asList(
                    "Breakfast: 9:00 - 11:00" +
                            "\nLunch: 12:00 - 15:00" +
                            "\nDoesn't work during the weekend."
                    ));
            ponuda = new ArrayList<String>(Arrays.asList(
                    "Menus\nChoice meals"
                    ));
            m_alu.setInfo(tip, radnoVrijeme, ponuda);
            object_list.add(m_alu);
        } // ALU
        { // TTF
            Canteen m_ttf = new Canteen("TTF", "Prilaz baruna Filipovića 28a", "Prilaz baruna Filipovića 28a",
                    new LatLng(45.812084, 15.937186), R.drawable.sc);
            tip = new ArrayList<String>(Arrays.asList(
                    "Blagavaona (Dining room)"
                    ));
            radnoVrijeme = new ArrayList<String>(Arrays.asList(
                    "8:00 - 14:00" +
                            "\nDoesn't work during the weekend."
                    ));
            ponuda = new ArrayList<String>(Arrays.asList(
                    "Sandwiches"
            ));
            m_ttf.setInfo(tip, radnoVrijeme, ponuda);
            object_list.add(m_ttf);
        } // TTF
        { // FER
            Canteen m_fer = new Canteen("FER", "Ulica grada Vukovara 39", "Ulica grada Vukovara 39",
                    new LatLng(45.800989, 15.970961), R.drawable.sc);
            tip = new ArrayList<String>(Arrays.asList(
                    "Restaurant 'Cassandra'",
                    "Express restaurant 'Cassandra'",
                    "Caffe bar 'Cassandra'"
            ));
            radnoVrijeme = new ArrayList<String>(Arrays.asList(
                    "10:00 – 16:00" +
                            "\nDoesn't work during the weekend.",
                    "8:00 – 19:00" +
                            "\nDoesn't work during the weekend.",
                    "Mon - Fri: 8:00 - 20:00" +
                            "\nSat: 8:00 - 16:00" +
                            "\nDoesn't work on Sundays."
            ));
            ponuda = new ArrayList<String>(Arrays.asList(
                    "Menus\nChoice meals",
                    "Sandwiches",
                    "Beverages"
            ));
            m_fer.setInfo(tip, radnoVrijeme, ponuda);
            object_list.add(m_fer);
        } // FER
        { // PBF
            Canteen m_pbf = new Canteen("PBF", "Pierottijeva 6", "Pierottijeva 6",
                    new LatLng(45.806891, 15.964395), R.drawable.sc);
            tip = new ArrayList<String>(Arrays.asList(
                    "Restaurant 'Superfaks'"
            ));
            radnoVrijeme = new ArrayList<String>(Arrays.asList(
                    "7:00 – 17:00" +
                            "\nDoesn't work during the weekend."
            ));
            ponuda = new ArrayList<String>(Arrays.asList(
                    "Menus\nChoice meals"
            ));
            m_pbf.setInfo(tip, radnoVrijeme, ponuda);
            object_list.add(m_pbf);
        } // PBF
        { // AFGF
            Canteen m_af = new Canteen("AF/GF", "Andrije Kačića Miošića 26", "Andrije Kačića Miošića 26",
                    new LatLng(45.808644, 15.963751), R.drawable.sc);
            tip = new ArrayList<String>(Arrays.asList(
                    "Restaurant 'Odeon'"
            ));
            radnoVrijeme = new ArrayList<String>(Arrays.asList(
                    "9:30 – 17:00" +
                            "\nDoesn't work during the weekend."
            ));
            ponuda = new ArrayList<String>(Arrays.asList(
                    "Menus\nBBQ meals"
            ));
            m_af.setInfo(tip, radnoVrijeme, ponuda);
            object_list.add(m_af);
        } // AFGF
        /*
        Canteen m_tvz = new Canteen("TVZ", "najbolji faks ikad?", "najbolji faks ikad?",
                new LatLng(45.800182, 15.928094), R.drawable.tvz);
        object_list.add(m_tvz);
        */
    }
	
}
