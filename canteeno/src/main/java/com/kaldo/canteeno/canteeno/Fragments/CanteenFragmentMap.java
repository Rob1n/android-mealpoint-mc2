package com.kaldo.canteeno.canteeno.Fragments;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.MarkerOptions;
import com.kaldo.canteeno.canteeno.Items.Canteen;
import com.kaldo.canteeno.canteeno.R;

/**
 * Created by Robin on 30.4.2014..
 */
public class CanteenFragmentMap extends SupportMapFragment {

    SupportMapFragment mSupportMapFragment;
    GoogleMap googleMap;
    Canteen canteen;

    CanteenFragmentMap(){}
    CanteenFragmentMap(Canteen canteen){
        this.canteen = canteen;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        super.onCreateView(inflater, container, savedInstanceState);
        return inflater.inflate(R.layout.canteen_info_map, null, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initializeMap();
    }

    private void initializeMap(){
        FragmentManager fm = getChildFragmentManager();
        mSupportMapFragment = (SupportMapFragment) fm.findFragmentById(R.id.canteen_actual_map);
        if (mSupportMapFragment == null) {
            mSupportMapFragment = SupportMapFragment.newInstance();
            fm.beginTransaction().replace(R.id.canteen_actual_map, mSupportMapFragment).commit();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (googleMap == null) {
            googleMap = mSupportMapFragment.getMap();
            googleMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(canteen.getLocation(), 17));
            googleMap.getUiSettings().setZoomControlsEnabled(false);
            googleMap.getUiSettings().setAllGesturesEnabled(false);
            googleMap.addMarker(new MarkerOptions()
                            //.title(canteen.getNaziv())
                            //.snippet(canteen.getSnippet())
                            .position(canteen.getLocation())
                            .draggable(false)
            );
        }
    }

}