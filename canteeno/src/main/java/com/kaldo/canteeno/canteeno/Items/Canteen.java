package com.kaldo.canteeno.canteeno.Items;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;

/**
 * Created by Robin on 21.4.2014..
 */
public class Canteen implements Parcelable{
    private String naziv, adresa;
    private ArrayList<String> tipKantine;
    private ArrayList<String> radnoVrijeme;
    private ArrayList<String> meni;
    private int imageId;

    private String distance;

    // marker info
    private String snippet;
    private LatLng location;

    public Canteen (String naziv, String adresa, String snippet, LatLng location, int imageId){
        this.naziv = naziv;
        this.adresa = adresa;
        this.tipKantine = new ArrayList<String>();
        this.radnoVrijeme = new ArrayList<String>();
        this.meni = new ArrayList<String>();
        this.snippet = snippet;
        this.location = location;
        this.imageId = imageId;
    }

    public Canteen (){
        this.naziv = null;
        this.adresa = null;
        this.tipKantine = new ArrayList<String>();
        this.radnoVrijeme = new ArrayList<String>();
        this.meni = new ArrayList<String>();
        this.snippet = null;
        this.location = null;
        this.imageId = 0;
    }

    public void setInfo (ArrayList<String> tipKantine, ArrayList<String> radnoVrijeme, ArrayList<String> meni){
        this.tipKantine = tipKantine;
        this.radnoVrijeme = radnoVrijeme;
        this.meni = meni;
    }

    //
    // parcelable stuff
    //

    @Override
    public int describeContents() {
        return 0;
    }

    // write your object's data to the passed-in Parcel
    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.naziv);
        parcel.writeString(this.adresa);
        parcel.writeStringList(this.tipKantine);
        parcel.writeStringList(this.radnoVrijeme);
        parcel.writeStringList(this.meni);
        parcel.writeString(this.snippet);
        parcel.writeDouble(this.location.latitude);
        parcel.writeDouble(this.location.longitude);
        parcel.writeInt(this.imageId);
    }

    // this is used to regenerate your object. All Parcelables must have a CREATOR that implements these two methods
    public static final Creator<Canteen> CREATOR = new Creator<Canteen>() {
        public Canteen createFromParcel(Parcel in) {
            return new Canteen(in);
        }

        public Canteen[] newArray(int size) {
            return new Canteen[size];
        }
    };

    // example constructor that takes a Parcel and gives you an object populated with it's values
    private Canteen(Parcel in) {
        this.naziv = in.readString();
        this.adresa = in.readString();
        this.tipKantine = in.createStringArrayList();
        this.radnoVrijeme = in.createStringArrayList();
        this.meni = in.createStringArrayList();
        this.snippet = in.readString();
        this.location = new LatLng(in.readDouble(), in.readDouble());
        this.imageId = in.readInt();
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public String getAdresa() {
        return adresa;
    }

    public void setAdresa(String adresa) {
        this.adresa = adresa;
    }

    public ArrayList<String> getRadnoVrijeme() {
        return radnoVrijeme;
    }

    public void setRadnoVrijeme(ArrayList<String> radnoVrijeme) {
        this.radnoVrijeme = radnoVrijeme;
    }

    public ArrayList<String> getMeni() {
        return meni;
    }

    public void setMeni(ArrayList<String> meni) {
        this.meni = meni;
    }

    public String getSnippet() {
        return snippet;
    }

    public void setSnippet(String snippet) {
        this.snippet = snippet;
    }

    public LatLng getLocation() {
        return location;
    }

    public void setLocation(LatLng location) {
        this.location = location;
    }

    public int getImageId() {
        return imageId;
    }

    public void setImageId(int imageId) {
        this.imageId = imageId;
    }

    public ArrayList<String> getTipKantine() {
        return tipKantine;
    }

    public void setTipKantine(ArrayList<String> tipKantine) {
        this.tipKantine = tipKantine;
    }


    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }
}
