package com.kaldo.canteeno.canteeno.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.kaldo.canteeno.canteeno.R;

import java.util.ArrayList;

/**
 * Created by Robin on 26.4.2014..
 */
public class CanteenInfoTextAdapter extends ArrayAdapter<String> {

    private ArrayList<String> tip;
    private ArrayList<String> radnoVrijeme;
    private ArrayList<String> meni;
    private LayoutInflater inflater;

    public CanteenInfoTextAdapter(Context context, int resource, ArrayList<String> tip,
                                  ArrayList<String> radnoVrijeme, ArrayList<String> meni) {
        super(context, 0, tip);
        this.tip = tip;
        this.radnoVrijeme = radnoVrijeme;
        this.meni = meni;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.canteen_info_item, null);
        }

        // init
        TextView info_TV1 = (TextView) convertView.findViewById(R.id.canteen_info_TV1);
        TextView info_TV3 = (TextView) convertView.findViewById(R.id.canteen_info_TV3);
        TextView info_TV5 = (TextView) convertView.findViewById(R.id.canteen_info_TV5);


        // data
        String newLine = getItem(position);
        info_TV1.setText(tip.get(position));
        info_TV3.setText(radnoVrijeme.get(position));
        info_TV5.setText(meni.get(position));
        notifyDataSetChanged();

        return convertView;
    }
}

