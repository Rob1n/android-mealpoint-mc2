package com.kaldo.canteeno.canteeno.Items;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by Robin on 28.4.2014..
 */
public class Ispis implements Parcelable {

    private ArrayList<String> food_list;
    private ArrayList<String> food_list_eng;

    public Ispis(){
        food_list = new ArrayList<String>();
        food_list_eng = new ArrayList<String>();
    }

    public Ispis(ArrayList<String> lista, ArrayList<String> lista_eng){
        this.food_list = new ArrayList<String>(lista);
        this.food_list_eng = new ArrayList<String>(lista_eng);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeStringList(this.food_list);
        parcel.writeStringList(this.food_list_eng);
    }

    public static final Creator<Ispis> CREATOR = new Creator<Ispis>() {
        public Ispis createFromParcel(Parcel in) {
            return new Ispis(in);
        }

        public Ispis[] newArray(int size) {
            return new Ispis[size];
        }
    };

    private Ispis(Parcel in){
        this.food_list = in.createStringArrayList();
        this.food_list_eng = in.createStringArrayList();
    }

    public ArrayList<String> getFood_list() {
        return food_list;
    }

    public void setFood_list(ArrayList<String> flist) {
        this.food_list = flist;
    }

    public ArrayList<String> getFood_list_eng() {
        return food_list_eng;
    }

    public void setFood_list_eng(ArrayList<String> food_list_eng) {
        this.food_list_eng = food_list_eng;
    }
}
