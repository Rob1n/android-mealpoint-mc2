package com.kaldo.canteeno.canteeno.Fragments;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.kaldo.canteeno.canteeno.Activity.GMapActivity;
import com.kaldo.canteeno.canteeno.Helpers.BaseContainerFragment;
import com.kaldo.canteeno.canteeno.Adapters.CanteenInfoTextAdapter;
import com.kaldo.canteeno.canteeno.Items.Canteen;
import com.kaldo.canteeno.canteeno.R;
import com.kaldo.canteeno.canteeno.Helpers.CollapseAnimation;

import java.util.ArrayList;

/**
 * Created by Robin on 21.4.2014..
 */
public class CanteenInfoFragment extends BaseContainerFragment implements View.OnClickListener {

    private Canteen canteen;

    private Button karta;

    private ListView listView;
    private ArrayList<String> tip;
    private ArrayList<String> radnoVrijeme;
    private ArrayList<String> ponuda;
    private CanteenInfoTextAdapter textAdapter;

    public CanteenInfoFragment(){}

    public CanteenInfoFragment(Canteen canteen){
        this.canteen = canteen;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.canteen_info, container, false);
        initView(view);
        return view;
    }

    private void replaceFragment(Fragment fragment) {
        ((BaseContainerFragment)getParentFragment()).replaceFragment(fragment, true);
    }

    private void initView(View view){
        // init
        TextView canteen_naziv = (TextView) view.findViewById(R.id.canteen_info_naziv);
        TextView canteen_adresa = (TextView) view.findViewById(R.id.canteen_info_adresa);
        Button btn_canteen_info_karta = (Button) view.findViewById(R.id.btn_canteen_info_karta);

        // data
        canteen_naziv.setText(canteen.getNaziv());
        canteen_adresa.setText(canteen.getAdresa());
        btn_canteen_info_karta.setOnClickListener(this);

        // map
        initMap(view);



        listView = (ListView) view.findViewById(R.id.canteen_info_tekst);
        tip = canteen.getTipKantine();
        radnoVrijeme = canteen.getRadnoVrijeme();
        ponuda = canteen.getMeni();

        textAdapter = new CanteenInfoTextAdapter(view.getContext(), 0, tip, radnoVrijeme, ponuda);
        listView.setAdapter(textAdapter);

        final Boolean[] arrow_up = {false, false, false, false, false};

        // collapse anim
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, final View view, int position, long id) {

                // changing the image
                ImageView arrow = (ImageView) view.findViewById(R.id.arrowImage);
                if (arrow_up[position] == false) {
                    arrow.setImageResource(R.drawable.xxdpi_icons_arrow_blue_rotated);
                    arrow_up[position] = true;
                } else {
                    arrow.setImageResource(R.drawable.xxdpi_icons_arrow_blue);
                    arrow_up[position] = false;
                }

                View toolbar = view.findViewById(R.id.collapseText);
                // stvaranje animacije
                CollapseAnimation expandAni = new CollapseAnimation(toolbar, 150);
                // pokretanje
                toolbar.startAnimation(expandAni);
            }
        });

        //button
        karta = (Button) view.findViewById(R.id.btn_canteen_info_karta);
        karta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (haveNetworkConnection()){
                    try {
                        Intent intent = new Intent(getActivity(), GMapActivity.class);
                        intent.putExtra("ParCanteen", canteen);
                        startActivity(intent);
                    } catch (Exception e){
                        Toast.makeText(view.getContext(), "GRESKA", Toast.LENGTH_LONG).show();
                    }
                }
                else {
                    Toast.makeText(getActivity(), "You need a working internet connection to use this service"
                            ,Toast.LENGTH_LONG).show();
                }

            }
        });
    }

    @Override
    public void onClick(View view) {
    }

    private boolean haveNetworkConnection() {
        //Log.i("CanteenInfoFragment", "checking for network connection");
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        return haveConnectedWifi || haveConnectedMobile;
    }

    private void initMap(View view){
        if (haveNetworkConnection()) {
            CanteenFragmentMap canteenFragmentMap = new CanteenFragmentMap(canteen);
            FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
            transaction.addToBackStack(null);
            transaction.replace(R.id.canteen_info_map_framelayout, canteenFragmentMap);
            transaction.commit();
        } else {
            LinearLayout ll = (LinearLayout) view.findViewById(R.id.canteen_info_banner);
            LayoutParams params = ll.getLayoutParams();
            params.height = 0;
        }
    }



}
