package com.kaldo.canteeno.canteeno.Activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;
import android.widget.TextView;

import com.kaldo.canteeno.canteeno.Adapters.PhrasebookFinalIspisAdapter;
import com.kaldo.canteeno.canteeno.Items.Ispis;
import com.kaldo.canteeno.canteeno.R;

import java.util.ArrayList;

public class IspisActivity extends Activity {

    private Ispis ispis;
    private ArrayList<String> food_list_cro;
    private ArrayList<String> food_list_eng;
    private ListView listView;
    private PhrasebookFinalIspisAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ispis);
        initView();
    }

    private void initView(){
        Intent intent = getIntent();
        this.ispis = (Ispis) intent.getParcelableExtra("IspisPonude");
        this.food_list_cro = this.ispis.getFood_list();
        this.food_list_eng = this.ispis.getFood_list_eng();

        listView = (ListView) findViewById(R.id.listView_finalni_ispis);
        adapter = new PhrasebookFinalIspisAdapter(this, 0, food_list_eng , food_list_cro);
        listView.setAdapter(adapter);

    }


}
