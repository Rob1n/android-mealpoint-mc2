package com.kaldo.canteeno.canteeno.Items;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Robin on 28.4.2014..
 */
public class Food implements Parcelable{

    private String ime;
    private String croIme;
    private boolean checked;
    private char category;

    public Food(){}

    public Food(String ime, char category, String croIme){
        this.ime = ime;
        this.croIme = croIme;
        this.category = category;
        this.checked = false;
    }

    public Food(String ime, char category, String croIme, boolean checked){
        this.ime = ime;
        this.croIme = croIme;
        this.category = category;
        this.checked = checked;
    }

    public Food(Food other){
        this.ime = other.getIme();
        this.croIme = other.getCroIme();
        this.category = other.getCategory();
        this.checked = other.isChecked();
    }

    // parcelable

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.ime);
        parcel.writeString(this.croIme);
        parcel.writeString(Character.toString(this.category));
        parcel.writeInt(this.checked ? 1 : 0);
    }

    public static final Creator<Food> CREATOR = new Creator<Food>() {
        public Food createFromParcel(Parcel in) {
            return new Food(in);
        }

        public Food[] newArray(int size) {
            return new Food[size];
        }
    };

    private Food(Parcel in){
        this.ime = in.readString();
        this.croIme = in.readString();
        this.category = in.readString().charAt(0);
        this.checked = (in.readInt() == 0) ? false : true;
    }


    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public char getCategory() {
        return category;
    }

    public void setCategory(char category) {
        this.category = category;
    }

    public String getCroIme() {
        return croIme;
    }

    public void setCroIme(String croIme) {
        this.croIme = croIme;
    }
}
